using System;
using System.Collections.Generic;
using System.Linq;

namespace Atividade05
{
    public class UserControl
    {
        private List<User> originalUsers = new List<User>();
        private List<User> users = new List<User>();
        private Stack<string> errorMessages = new Stack<string>();

        public delegate void UserInsertEventHandler();
        public event UserInsertEventHandler? UserInsert;

        public List<User> Users
        {
            get { return users; }
        }

        public void AddUser(User user)
        {
            try
            {
                originalUsers.Add(user);
                users.Add(user);
                OnUserInsert("Adicionar Usuário");
            }
            catch (Exception ex)
            {
                errorMessages.Push($"Erro ao adicionar usuário: {ex.Message}");
            }
        }

        public void SortUsersAscending()
        {
            try
            {
                users = users.OrderBy(u => u.Email).ToList();
                OnUserInsert("Ordenar Crescente");
            }
            catch (Exception ex)
            {
                errorMessages.Push($"Erro ao ordenar usuarios em ordem crescente: {ex.Message}");
            }
        }

        public void SortUsersDescending()
        {
            try
            {
                users = users.OrderByDescending(u => u.Email).ToList();
                OnUserInsert("Ordenar Decrescente");
            }
            catch (Exception ex)
            {
                errorMessages.Push($"Erro ao ordenar usuarios em ordem decrescente: {ex.Message}");
            }
        }

        public void SearchUsersByEmail(string email)
        {
            try
            {
                users = originalUsers
                    .Where(u => u.Email.Contains(email, StringComparison.OrdinalIgnoreCase))
                    .ToList();

                OnUserInsert($"Pesquisar por Email: {email}");
            }
            catch (Exception ex)
            {
                errorMessages.Push($"Erro ao pesquisar usuarios por e-mail: {ex.Message}");
            }
        }

            public void DeleteAllUsers()
            {
                try
                {
                    originalUsers.Clear();
                    users.Clear();
                    OnUserInsert("Excluir Todos os usuarios");
                }
                catch (Exception ex)
                {
                    errorMessages.Push($"Erro ao excluir todos os usuarios: {ex.Message}");
                }
            }

        protected virtual void OnUserInsert(string action)
        {
            UserInsert?.Invoke();
            DisplayNotification($"Action: {action}");
        }

        private void DisplayNotification(string message)
        {
            Console.WriteLine(message);
        }
    }
}