public class User
{
    public string Nome { get; set; }
    public string Sobrenome { get; set; }
    public string Email { get; set; }

    public User(string nome, string sobrenome, string email)
    {
        Nome = nome;
        Sobrenome = sobrenome;
        Email = email;
    }
}

