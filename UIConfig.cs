using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Atividade05
{
    public static class UIConfig
    {
        public static void InitializeUI(Form1 form, TextBox firstName, TextBox lastName, TextBox email, ListView listUsers, Button sortAscButton, Button sortDescButton, TextBox searchEmail, Button searchButton, Label summaryLabel, Label notificationLabel, Button showErrorButton, Button deleteAllButton)
        {
            TableLayoutPanel tableLayoutPanel = new TableLayoutPanel();

            Label labelFirstName = new Label();
            Label labelLastName = new Label();
            Label labelEmail = new Label();

            labelFirstName.Text = "Nome:";
            labelFirstName.TextAlign = ContentAlignment.MiddleLeft;
            tableLayoutPanel.Controls.Add(labelFirstName, 0, 0);

            firstName.Dock = DockStyle.Left;
            tableLayoutPanel.Controls.Add(firstName, 1, 0);

            labelLastName.Text = "Sobrenome:";
            labelLastName.TextAlign = ContentAlignment.MiddleLeft;
            tableLayoutPanel.Controls.Add(labelLastName, 0, 1);

            lastName.Dock = DockStyle.Left;
            tableLayoutPanel.Controls.Add(lastName, 1, 1);

            labelEmail.Text = "E-mail:";
            labelEmail.TextAlign = ContentAlignment.MiddleLeft;
            tableLayoutPanel.Controls.Add(labelEmail, 0, 2);

            email.Dock = DockStyle.Left;
            tableLayoutPanel.Controls.Add(email, 1, 2);

            Button buttonSend = new Button();
            buttonSend.Location = new System.Drawing.Point(500, 0);
            buttonSend.Size = new System.Drawing.Size(90, 30);
            buttonSend.Text = "Enviar";
            buttonSend.Click += (sender, e) => form.buttonSend_Click(sender, e);
            tableLayoutPanel.Controls.Add(buttonSend, 2, 1);

            tableLayoutPanel.Size = new System.Drawing.Size(600, 250);

            sortAscButton.Size = new System.Drawing.Size(90, 30);
            sortAscButton.Text = "Crescente";
            tableLayoutPanel.Controls.Add(sortAscButton, 0, 3);

            sortDescButton.Size = new System.Drawing.Size(100, 30);
            sortDescButton.Text = "Decrescente";
            tableLayoutPanel.Controls.Add(sortDescButton, 1, 3);

            Label labelSearch = new Label();
            labelSearch.Text = "Pesquisar  Email:";
            labelSearch.TextAlign = ContentAlignment.MiddleLeft;
            labelSearch.Size = new System.Drawing.Size(120, 30);

            listUsers.Size = new System.Drawing.Size(590, 150);
            tableLayoutPanel.Controls.Add(labelSearch, 2, 3);

            searchEmail.Dock = DockStyle.Left;
            searchEmail.Size = new System.Drawing.Size(100, 20);
            tableLayoutPanel.Controls.Add(searchEmail, 3, 3);

            searchButton.Size = new System.Drawing.Size(90, 30);
            searchButton.Text = "Pesquisar";
            tableLayoutPanel.Controls.Add(searchButton, 4, 3);

            listUsers.Location = new System.Drawing.Point(10, 90);
            listUsers.Size = new System.Drawing.Size(590, 150);
            listUsers.View = View.Details;
            listUsers.Columns.Add("Nome", 150);
            listUsers.Columns.Add("Sobrenome", 150);
            listUsers.Columns.Add("E-mail", 150);
            tableLayoutPanel.Controls.Add(listUsers, 0, 4);
            tableLayoutPanel.SetColumnSpan(listUsers, 5);

            summaryLabel.Dock = DockStyle.Bottom;
            tableLayoutPanel.Controls.Add(summaryLabel, 0, 5);
            tableLayoutPanel.SetColumnSpan(summaryLabel, 5);
            notificationLabel.Dock = DockStyle.Bottom;
            tableLayoutPanel.Controls.Add(notificationLabel, 0, 6);
            tableLayoutPanel.SetColumnSpan(notificationLabel, 5);

            sortAscButton.Click += (sender, e) => form.userControl.SortUsersAscending();
            sortDescButton.Click += (sender, e) => form.userControl.SortUsersDescending();
            searchButton.Click += (sender, e) => form.userControl.SearchUsersByEmail(searchEmail.Text);

            // Adicione o botão "Mostrar Logs" no meio do lado direito
            showErrorButton.Size = new System.Drawing.Size(90, 30);
            showErrorButton.Text = "Mostrar Logs";
            showErrorButton.Click += (sender, e) => form.ShowErrorMessages();
            tableLayoutPanel.Controls.Add(showErrorButton, 2, 4);

            // Adicione o botão "Apagar Tudo" no meio do lado direito
            deleteAllButton.Size = new System.Drawing.Size(90, 30);
            deleteAllButton.Text = "Apagar Tudo";
            deleteAllButton.Click += (sender, e) => form.userControl.DeleteAllUsers();
            tableLayoutPanel.Controls.Add(deleteAllButton, 3, 4);

            tableLayoutPanel.Dock = DockStyle.Left;
            tableLayoutPanel.AutoSize = true;
            tableLayoutPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            form.Controls.Add(tableLayoutPanel);
        }
    }
}