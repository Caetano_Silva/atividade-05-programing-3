using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Atividade05
{
    public partial class Form1 : Form
    {
        private TextBox firstName;
        private TextBox lastName;
        private TextBox email;
        private ListView listUsers;
        private Button ascButton;
        private Button descButton;
        private TextBox searchEmail;
        private Button searchButton;
        private Label summaryLabel;
        private Label notificationLabel;
        private Button showErrorButton;
        private Button deleteAllButton;

        public UserControl userControl { get; set; }
        private Stack<string> errorMessages = new Stack<string>();

        public Form1()
        {
            InitializeComponent();

            userControl = new UserControl();
            userControl.UserInsert += UpdateListView;
            firstName = new TextBox();
            lastName = new TextBox();
            email = new TextBox();
            listUsers = new ListView();
            ascButton = new Button();
            descButton = new Button();
            searchEmail = new TextBox();
            searchButton = new Button();
            summaryLabel = new Label();
            notificationLabel = new Label();
            showErrorButton = new Button();
            deleteAllButton = new Button();

            showErrorButton.Click += (sender, e) => ShowErrorMessages();
            deleteAllButton.Click += (sender, e) => DeleteAllUsers();

            UIConfig.InitializeUI(this, firstName, lastName, email, listUsers, ascButton, descButton, searchEmail, searchButton, summaryLabel, notificationLabel, showErrorButton, deleteAllButton);
        }

        public void buttonSend_Click(object sender, EventArgs e)
        {
            string nome = firstName?.Text ?? "";
            string sobrenome = lastName?.Text ?? "";
            string userEmail = email?.Text ?? "";

            if (string.IsNullOrEmpty(nome) || string.IsNullOrEmpty(sobrenome) || string.IsNullOrEmpty(userEmail))
            {
                LogErrorMessage("Todos os campos são obrigatórios.");
                return;
            }

            if (!IsValidEmail(userEmail))
            {
                LogErrorMessage("Endereço de email inválido.");
                return;
            }

            User newUser = new User(nome, sobrenome, userEmail);
            userControl.AddUser(newUser);

            notificationLabel?.Invoke(new Action(() =>
            {
                notificationLabel.Text = $"Novo usuário adicionado: {nome} {sobrenome}";
            }));
        }

        internal void UpdateListView()
        {
            if (userControl != null && listUsers != null)
            {
                listUsers.Items.Clear();

                var users = userControl.Users;

                foreach (var user in users)
                {
                    ListViewItem item = new ListViewItem(new string[] { user.Nome, user.Sobrenome, user.Email });
                    listUsers.Items.Add(item);
                }

                UpdateSummaryLabel(users.Count);
            }
        }

        private void UpdateSummaryLabel(int totalUsers)
        {
            summaryLabel.Text = $"Total de usuários: {totalUsers}";
        }

        public void ShowErrorMessages()
        {
            List<string> formattedErrorMessages = new List<string>();
            foreach (string error in errorMessages)
            {
                string formattedMessage = $"{error}";
                formattedErrorMessages.Add(formattedMessage);
            }

            string errorMessage = string.Join(Environment.NewLine, formattedErrorMessages);
            MessageBox.Show(errorMessage, "Error Messages", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void DeleteAllUsers()
        {
            userControl.DeleteAllUsers();
            UpdateListView();
            notificationLabel.Text = "Todos os usuários foram excluídos.";
        }

        private void LogErrorMessage(string errorMessage)
        {
            string formattedErrorMessage = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss} - {errorMessage}";
            errorMessages.Push(formattedErrorMessage);
            ShowErrorMessages();
        }

        private void ValidateEmail()
        {
            string userEmail = email?.Text ?? "";
            if (!IsValidEmail(userEmail))
            {
                LogErrorMessage("Endereço de email inválido.");
            }
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}