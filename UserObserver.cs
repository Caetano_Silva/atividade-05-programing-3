namespace Atividade05
{
    public class UserObserver : InterfaceObserver
    {
        private Form1 form;

        public UserObserver(Form1 form)
        {
            this.form = form;
        }

        public void Update()
        {
            form.Invoke(new Action(() => form.UpdateListView()));
        }
    }
}
